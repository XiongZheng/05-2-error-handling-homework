package com.twuc.webApp.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class MazeControllerException {

    @ExceptionHandler
    public ResponseEntity get(IllegalArgumentException e){
        return ResponseEntity.status(400).body("message in exception");
    }
}
